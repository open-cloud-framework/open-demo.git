package com.open.demo.function;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * 检查
 *
 * @author zhang kaichao
 * @since 2021/11/29 10:42:13
 */
public interface IFunction {

    /**
     * 检查
     *
     * @param checkConsumer 消费
     * @param <T>类型
     * @return Function
     */
    default <T> Function<T, T> buildCheck(Consumer<T> checkConsumer) {
        return (checkContext) -> {
            checkConsumer.accept(checkContext);
            return checkContext;
        };
    }

    /**
     * 添加失败信息
     *
     * @param condition 是否添加失败消息 true添加  false不添加
     * @param context   检测失败返回消息
     * @param message   失败消息
     */
    default void addFailedMessage(Boolean condition, CheckDetailContext context, String message) {
        List<String> failMessages = context.getFailedMessages();
        if (CollectionUtils.isEmpty(failMessages)) {
            context.setFailedMessages(new ArrayList<>());
        }
        if (condition) {
            context.setIsPass(Boolean.FALSE);
            context.getFailedMessages().add(message);
        }
    }
}
