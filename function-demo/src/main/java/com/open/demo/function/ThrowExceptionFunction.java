package com.open.demo.function;

/**
 * 定义一个抛出异常的函数式接口, 消费型接口:只有参数没有返回值
 *
 * @author zhang kaichao
 * @since 2021/11/24 14:16:58
 */
@FunctionalInterface
public interface ThrowExceptionFunction {

    /**
     * 抛出异常信息
     * @param message 异常信息
     */
    void throwMessage(String message);
}
