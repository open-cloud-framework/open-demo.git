package com.open.demo.function;

import cn.hutool.core.lang.Assert;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 检测详情连续性
 *
 * @author zhang kaichao
 * @since 2021/11/29 10:44:13
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckDetailContext {

    /**
     * 检测失败原因
     */
    @Builder.Default
    private List<String> failedMessages = new ArrayList<>();

    @Builder.Default
    private Boolean isPass = Boolean.TRUE;

    /**
     * 检测
     */
    public void check(){
        Boolean isCheck = getIsPass();
        Assert.isTrue(isCheck, getFailedMessages().toString());
    }
}
