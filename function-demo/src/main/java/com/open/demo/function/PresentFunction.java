package com.open.demo.function;

import java.util.function.Consumer;

/**
 * 定义一个PresentFunction的函数式接口，接口的参数一个为Consumer接口。一个为Runnable,分别代表值不为空时执行消费操作和值为空时执行的其他操作
 *
 * @author zhang kaichao
 * @since 2021/11/24 15:48:13
 */
public interface PresentFunction<T extends Object> {

    /**
     * 值不为空时执行消费操作
     * 值为空时执行其他的操作
     *
     * @param action      action 值不为空时，执行的消费操作
     * @param emptyAction emptyAction 值为空时，执行的操作
     */
    void presentHandle(Consumer<? super T> action, Runnable emptyAction);
}
