package com.open.demo;

import com.open.demo.function.CheckDetailContext;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author zhang kaichao
 * @since 2021/11/29 10:50:03
 */
@SpringBootTest(classes = CheckFunctionTest.class)
public class CheckFunctionTest {

    @Test
    public void checkTest() {
        AppInfoCheckFunction appInfoCheckFunction = new AppInfoCheckFunction();
        appInfoCheckFunction.checkTest()
                .andThen(appInfoCheckFunction.checkTest2())
                .apply(CheckDetailContext.builder().build())
                .check();
    }
}
