package com.open.demo;

import com.open.demo.function.CheckDetailContext;
import com.open.demo.function.IFunction;
import com.sun.deploy.ui.AppInfo;
import org.springframework.util.StringUtils;

import java.util.function.Function;

/**
 * @author zhang kaichao
 * @since 2021/11/29 10:50:48
 */
public class AppInfoCheckFunction implements IFunction {

    public Function<CheckDetailContext, CheckDetailContext> checkTest() {
        return buildCheck(res -> {
            String testName1 = "";
            String testName2 = "test";
            addFailedMessage(!StringUtils.isEmpty(testName1), res, String.format("testName1 [%s] 为空", testName1));
            addFailedMessage(!"test".equals(testName2), res, String.format("testName2 [%s] 已存在",testName2));
        });
    }

    public Function<CheckDetailContext, CheckDetailContext> checkTest2() {
        return buildCheck(res -> {
            String testName3 = "test3";
            addFailedMessage("test3".equals(testName3), res, String.format("testName3 [%s] 已存在",testName3));
        });
    }


}
