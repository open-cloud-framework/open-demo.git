package com.open.demo;

import com.open.demo.function.FuncUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * FunctionTest
 *
 * @author zhang kaichao
 * @since 2021/11/24 14:51:14
 */
@SpringBootTest(classes = FunctionTest.class)
public class FunctionTest {

    @Test
    public void throwFalseTest() {
        FuncUtil.isTure(false).throwMessage("测试异常");
    }

    @Test
    public void throwTrueTest() {
        FuncUtil.isTure(true).throwMessage("测试异常");
    }

    @Test
    public void branchFalseTest() {
        FuncUtil.isTureOrFalse(false)
                .trueOrFalseHandle(() -> System.out.println("true 逻辑"), () -> System.out.println("false 逻辑"));
    }

    @Test
    public void branchTrueTest() {
        FuncUtil.isTureOrFalse(true)
                .trueOrFalseHandle(() -> System.out.println("true 逻辑"), () -> System.out.println("false 逻辑"));
    }

    @Test
    public void presentBlankTest(){
        FuncUtil.isBlankOrNoBlank("")
                .presentHandle(System.out::println,
                        ()-> System.out.println("空字符串"));
    }

    @Test
    public void presentNonBlankTest(){
        FuncUtil.isBlankOrNoBlank("test")
                .presentHandle(System.out::println,
                        ()-> System.out.println("空字符串"));
    }


}
