package com.open.demo.eventing.test;

import com.open.demo.eventing.DefaultStateMachineConfig;
import com.open.demo.eventing.ProcessContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Since: 1.0.0
 * @Author: Kevin.Zhang
 **/
@SpringBootTest
public class MachineTest {

    @Autowired
    private DefaultStateMachineConfig defaultStateMachineConfig;

    @Test
    public void checkTest() {
        ProcessContext processContext = new ProcessContext();
        processContext.setName("测试");
        defaultStateMachineConfig.getProcessCtrlEventPublisher().publish(processContext);
    }
}
