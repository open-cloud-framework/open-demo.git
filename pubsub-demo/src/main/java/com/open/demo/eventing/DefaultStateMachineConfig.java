package com.open.demo.eventing;

import com.open.demo.eventing.impl.DirectEventBus;
import com.open.demo.eventing.impl.ProcessCtrlEventConsumer;
import com.open.demo.eventing.impl.ProcessCtrlEventPublisher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @since 1.0.0
 * @author Kevin.Zhang
 **/
@Component
public class DefaultStateMachineConfig implements ApplicationContextAware, InitializingBean {

    private ProcessCtrlEventPublisher syncProcessCtrlEventPublisher;
    private ApplicationContext applicationContext;

    protected void init() throws Exception {

        if (syncProcessCtrlEventPublisher == null) {
            ProcessCtrlEventPublisher syncEventPublisher = new ProcessCtrlEventPublisher();

            ProcessCtrlEventConsumer processCtrlEventConsumer = new ProcessCtrlEventConsumer();

            DirectEventBus directEventBus = new DirectEventBus();
            syncEventPublisher.setEventBus(directEventBus);

            directEventBus.registerEventConsumer(processCtrlEventConsumer);

            syncProcessCtrlEventPublisher = syncEventPublisher;
        }

    }


    @Override
    public void afterPropertiesSet() throws Exception {
        init();
    }

    public ProcessCtrlEventPublisher getProcessCtrlEventPublisher() {
        return syncProcessCtrlEventPublisher;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
