/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.open.demo.eventing.impl;

import com.open.demo.eventing.EventBus;
import com.open.demo.eventing.EventPublisher;
import com.open.demo.eventing.ProcessContext;


/**
 * @since 1.0.0
 * @author Kevin.Zhang
 **/
public class ProcessCtrlEventPublisher implements EventPublisher<ProcessContext> {

    private EventBus<ProcessContext> eventBus;

    @Override
    public boolean publish(ProcessContext event) {
        System.out.println("ProcessCtrlEventPublisher publish");
        return eventBus.offer(event);
    }

    public void setEventBus(EventBus<ProcessContext> eventBus) {
        System.out.println("ProcessCtrlEventPublisher setEventBus");
        this.eventBus = eventBus;
    }
}