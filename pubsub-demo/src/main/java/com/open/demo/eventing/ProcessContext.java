package com.open.demo.eventing;

/**
 * @since 1.0.0
 * @author Kevin.Zhang
 **/
public class ProcessContext {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
