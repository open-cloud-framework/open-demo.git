package com.open.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @since 1.0.0
 * @author Kevin.Zhang
 **/
@SpringBootApplication
public class EventingApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventingApplication.class, args);
    }
}
